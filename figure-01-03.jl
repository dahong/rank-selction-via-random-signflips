### A Pluto.jl notebook ###
# v0.19.40

using Markdown
using InteractiveUtils

# ╔═╡ 4acaacbd-196e-428b-b508-0257c2e82bca
# ╠═╡ show_logs = false
import Pkg; Pkg.activate(@__DIR__); Pkg.instantiate()

# ╔═╡ 3f7de967-4f0c-43de-b7d3-b3fa32548079
using CairoMakie, LinearAlgebra, Random, StableRNGs, Statistics

# ╔═╡ 9b4bb7be-252b-494f-b979-1789941a83bb
md"""
# Figures 1-3
"""

# ╔═╡ 9c736d91-3df0-4032-8b97-f12271c773c9
PLOT_THEME = Theme(;
	fontsize = 16f0,
	Heatmap  = (; colormap=:vik, colorrange=(-0.35, 0.35)),
	Legend   = (; framevisible=false, padding=(0,0,0,0), rowgap=0),
)

# ╔═╡ 7eb990bc-7c5f-4d1c-8229-ea0189397b64
md"""
## Utility functions
"""

# ╔═╡ 4a55c614-065c-4208-bdbc-df82930f030a
import FlipPA

# ╔═╡ 3577f9d8-0d3b-4f92-b3c9-561bd3b45aee
function Arrow(figpos, coords; linewidth=5, arrowsize=10, color=Cycled(1))
	# Create axis
	ax = Axis(figpos)

	# Shorten final line segment for arrow head (error if too short)
	delta = coords[end] .- coords[end-1]
	arrowsize < norm(delta)/2 ||
		error("Arrow head is too large, need `arrowsize < $(norm(delta)/2)`")
	scoords = [coords[1:end-1]; coords[end] .- arrowsize.*(delta./norm(delta))]

	# Draw arrow
	scatter!(ax, scoords[end]...; space=:pixel, markersize=2*arrowsize,
		marker=:utriangle, rotations=-atan(delta...), color)
	lines!(ax, scoords; space=:pixel, linewidth, color)

	# Hide axis and return
	hidedecorations!(ax)
	hidespines!(ax)
	return ax
end

# ╔═╡ 9bb27ce0-bf0a-4195-a319-e4e78827b1fd
function MatrixViz(figpos, matrix; unit_per_entry=2,
	title="", titlesize=16f0, titlepadding=5,
	rowlabel="", columnlabel="", labelsize=12f0, labelpadding=(4,4))

	# Plot heatmap
	grid = GridLayout(figpos)
	ax = Axis(grid[1,1]; yreversed=true, aspect=DataAspect())
	hm = heatmap!(ax, permutedims(matrix))
	rowsize!(grid, 1, Fixed(size(matrix,1)*unit_per_entry))
	colsize!(grid, 1, Fixed(size(matrix,2)*unit_per_entry))

	# Add title and labels
	if !isempty(title)
		Label(grid[1,1,Bottom()], title; valign=:top,
			padding=(0,0,0,titlepadding), fontsize=titlesize)
	end
	if !isempty(rowlabel)
		Label(grid[1,1,Left()], "⟵ $rowlabel"; valign=:top, rotation=pi/2,
			padding=(0,labelpadding[2],0,labelpadding[1]), fontsize=labelsize)
	end
	if !isempty(columnlabel)
		Label(grid[1,1,Top()], "$columnlabel ⟶"; halign=:left,
			padding=(labelpadding[1],0,labelpadding[2],0), fontsize=labelsize)
	end

	# Hide axis and return
	hidedecorations!(ax)
	hidespines!(ax)
	return grid, ax, hm
end

# ╔═╡ ee2f358b-f4da-47cb-bcc9-576d6126496f
function pa_scree!(ax, σ, σt, q=0.95, τ=0.0; label="data", op_label="transformed",
	target_rank=nothing, arrow_angle=40, arrow_offset=0.3, arrow_length=0.4,
	annotation_position=(0.3, 0.9), annotation_size=18f0)

	## Plot the data singular values
	scatterlines!(ax, σ; label="original $label")

	## Plot the parallel singular values and their corresponding quantiles
	for k in 1:length(σ)
		violin!(ax, fill(k, length(σt)), getindex.(σt, k);
			side=:left, width=1.5, color=Cycled(2))
	end
	scatterlines!(ax, [quantile(getindex.(σt, k), q) for k in 1:length(σ)];
		label="$op_label $label")

	## Show selected vs target rank
	if !isnothing(target_rank)
		nselected = FlipPA._compare_svdvals(σ, σt, q, τ, FlipPA.Pairwise())

		# Draw arrows for the selected rank
		arrows!(ax,
			(1:nselected)  .+ arrow_offset*cosd(arrow_angle),
			σ[1:nselected] .+ arrow_offset*sind(arrow_angle),
			-fill(cosd(arrow_angle),nselected), -fill(sind(arrow_angle),nselected);
			color=[fill(:green,target_rank); fill(:red,length(σ)-1)][1:nselected],
			align=:center, lengthscale=arrow_length)

		# Add annotation for the selected rank
		text!(ax, annotation_position...; space=:relative, align=(:center, :top),
			text="$nselected selected", fontsize=annotation_size,
			color=(nselected == target_rank ? :green : :red))
	end

	return ax
end

# ╔═╡ 6a73f65c-15dc-4db8-9de2-c69cebe79070
md"""
## Generate signal and noise matrices
"""

# ╔═╡ 4789b052-974d-47a0-bcc8-9d327f2036d3
n, p = 50, 80

# ╔═╡ c39b048b-3ede-432e-b9bd-7286cfcfd24a
θ = 3

# ╔═╡ ae08f2c9-dff8-4cac-a4d7-cafc557e4d48
u = [ones(n÷2); -ones(n÷2)]/sqrt(n);

# ╔═╡ b2bdee21-248d-4c1f-b211-9aaf28fc727a
z = [ones(p÷2); -ones(p÷2)]/sqrt(p);

# ╔═╡ d4999823-4efe-4719-a47c-f7e306c3a1f2
S = θ*u*z';

# ╔═╡ 92df7c73-5aed-4102-a58a-daeb4759648c
Nhom = randn(StableRNG(1), n, p) .* sqrt(1/n);

# ╔═╡ 620a62be-337b-4c60-af38-9105e0d383ff
Nhet = let
	V = [
		1/14 * ones(n÷10*7, p÷4*3) ones(n÷10*7, p÷4*1);
		19/6 * ones(n÷10*3, p÷4*3) ones(n÷10*3, p÷4*1);
	]
	randn(StableRNG(1), n, p) .* sqrt.(V/n)
end;

# ╔═╡ 6f9a6971-825c-48ef-b5bb-16afffdc9ef3
md"""
## Figure 1
"""

# ╔═╡ 55824f36-bde6-4359-b18e-9b23761f35cf
figure_01 = map([Nhom, Nhet]) do N
	with_theme(PLOT_THEME) do
		fig = Figure()

		# Compute data matrix
		X = S + N

		# Visualize data, signal, and noise matrices
		grid = GridLayout(fig[1,1])
		MatrixViz(grid[1,1], X; title="data",
			rowlabel="observations", columnlabel="features")
		Label(grid[1,2], "="; tellheight=false, fontsize=32f0)
		MatrixViz(grid[1,3], S; title="signal")
		Label(grid[1,4], "+"; tellheight=false, fontsize=32f0)
		MatrixViz(grid[1,5], N; title="noise")
		colgap!(grid, 10)

		# Add arrow
		ax = Arrow(fig[1,2], [(0, n), (70, n)]; color=:hotpink)
		text!(ax, 30, n; space=:pixel, align=(:center, :baseline), offset=(0, 10),
			text="PermPA", fontsize=16f0, color=:hotpink)
		colsize!(fig.layout, 2, Fixed(70))

		# Plot the singular values
		ax = Axis(fig[1,3]; ylabel="singular values")
		pa_scree!(ax, svdvals(X),
			[svdvals(mapslices(x -> shuffle(rng, x), X; dims=1))
				for rng in StableRNG.(1:1000)];
			op_label="permuted", target_rank=1)
		axislegend(ax; labelsize=14f0)
		ax.limits = ((0.25, 12.5), (1.4, 4.1))
		ax.xticks = 1:12
		ax.yticks = 0:0.5:5
		colsize!(fig.layout, 3, Fixed(260))

		# Layout
		colgap!(fig.layout, 10)
		resize_to_layout!(fig)

		fig
	end
end

# ╔═╡ 3b1084c0-862d-4b27-89b2-fa8abed05e06
save.(["figure-01a.png", "figure-01b.png"], figure_01)

# ╔═╡ 3a4cc2b2-ee6a-497c-a060-7d10c13a3edb
md"""
## Figure 2
"""

# ╔═╡ 4995982d-90b5-4fe9-862b-4b5ba88b23b8
figure_02 = with_theme(PLOT_THEME; linewidth=2) do
	fig = Figure()

	## Compute data matrix
	N = Nhet
	X = S + N

	## Visualize data, signal, and noise matrices
	grid = GridLayout(fig[1,1])
	MatrixViz(grid[1,1], X; unit_per_entry=3, titlesize=18f0, title="data",
		labelsize=16f0, rowlabel="observations", columnlabel="features")
	Label(grid[1,2], "="; tellheight=false, fontsize=48f0)
	MatrixViz(grid[1,3], S; unit_per_entry=3, titlesize=18f0, title="signal")
	Label(grid[1,4], "+"; tellheight=false, fontsize=48f0)
	MatrixViz(grid[1,5], N; unit_per_entry=3, titlesize=18f0, title="noise")
	colgap!(grid, 10)

	## Visualize FlipPA
	gridflip = GridLayout(fig[2,1])

	# Add transformation arrow
	ax = Arrow(gridflip[1,1], [(25, 3n), (25, 1.5n), (105, 1.5n)]; color=:hotpink)
	text!(ax, 60, 1.5n; space=:pixel, align=(:center, :top), offset=(0, -10),
		text="signflip", fontsize=18f0, color=:hotpink)
	colsize!(gridflip, 1, Fixed(105))

	# Visualize transformed data
	MatrixViz(gridflip[1,2], X .* rand(StableRNG(1), (-1,1), size(X));
		unit_per_entry=3, titlesize=18f0, title="signflipped data")

	# Add repetition arrow
	ax = Arrow(gridflip[1,3], [(0, 1.5n), (70, 1.5n)]; color=:hotpink)
	text!(ax, 30, 1.5n; space=:pixel, align=(:center, :top), offset=(0, -10),
		text="repeat\nT times", fontsize=18f0, color=:hotpink)
	colsize!(gridflip, 3, Fixed(70))

	# Plot the singular values
	ax = Axis(gridflip[1,4]; ylabel="singular values")
	pa_scree!(ax, svdvals(X),
		[svdvals(X .* rand(StableRNG(t), (-1,1), size(X))) for t in 1:1000];
		op_label="signflipped", target_rank=1)
	axislegend(ax)
	ax.limits = ((0.25, 12.5), (1.4, 4.1))
	ax.xticks = 1:12
	ax.yticks = 0:0.5:5

	## Layout
	colgap!(gridflip, 10)
	resize_to_layout!(fig)

	fig
end

# ╔═╡ 01a5b902-6b4b-48a3-91be-dcef8067d5c2
save("figure-02.png", figure_02)

# ╔═╡ c0e187a9-c5e5-4538-8438-c140c6677724
md"""
## Figure 3
"""

# ╔═╡ 7710f8cb-c71d-4ac6-b755-39c9c270ba8d
figure_03 = with_theme(PLOT_THEME) do
	fig = Figure()

	## Compute data matrix
	N = Nhet
	X = S + N

	## Visualize matrices and singular values
	for (col, (M, label)) in [1 => (X, "data"), 3 => (S, "signal"), 5 => (N, "noise")]
		# Visualize original matrices
		MatrixViz(fig[1,col], M; title=label,
			rowlabel    = (col == 1 ? "observations" : ""),
			columnlabel = (col == 1 ? "features" : ""))

		# Add arrow
		ax = Arrow(fig[2,col], [(p, 40), (p, 0)]; color=:hotpink)
		text!(ax, p, 28; space=:pixel, align=(:left, :center), offset=(10, 0),
			text="signflip", fontsize=16f0, color=:hotpink)
		rowsize!(fig.layout, 2, Fixed(40))

		# Visualize signflipped matrices
		MatrixViz(fig[3,col], M .* rand(StableRNG(1), (-1,1), size(M));
			title="signflipped $label",
			rowlabel    = (col == 1 ? "observations" : ""),
			columnlabel = (col == 1 ? "features" : ""))

		# Plot the singular values
		ax = Axis(fig[4,col]; ylabel="singular values")
		pa_scree!(ax, svdvals(M),
			[svdvals(M .* rand(StableRNG(t), (-1,1), size(M))) for t in 1:1000];
			label, op_label="signflipped")
		axislegend(ax; labelsize=12f0, margin=(0,5,0,0))
		ax.limits = ((0.25, 10.5), (0.0, 5.1))
		ax.xticks = 1:16
		ax.yticks = 0:1:5
		col == 1 || hideydecorations!(ax; ticks=false, grid=false)
		rowsize!(fig.layout, 4, Fixed(100))
	end

	## Add math symbols
	Label(fig[1,2], "="; tellheight=false, fontsize=32f0)
	Label(fig[1,4], "+"; tellheight=false, fontsize=32f0)
	Label(fig[3,2], "="; tellheight=false, fontsize=32f0)
	Label(fig[3,4], "+"; tellheight=false, fontsize=32f0)

	## Layout
	colgap!(fig.layout, 10)
	rowgap!(fig.layout, 10)
	rowgap!(fig.layout, 3, 25)
	resize_to_layout!(fig)

	fig
end

# ╔═╡ 2fae4b02-57fb-44d9-ad3b-e3aaede3632e
save("figure-03.png", figure_03)

# ╔═╡ Cell order:
# ╟─9b4bb7be-252b-494f-b979-1789941a83bb
# ╠═4acaacbd-196e-428b-b508-0257c2e82bca
# ╠═3f7de967-4f0c-43de-b7d3-b3fa32548079
# ╠═9c736d91-3df0-4032-8b97-f12271c773c9
# ╟─7eb990bc-7c5f-4d1c-8229-ea0189397b64
# ╠═4a55c614-065c-4208-bdbc-df82930f030a
# ╟─3577f9d8-0d3b-4f92-b3c9-561bd3b45aee
# ╟─9bb27ce0-bf0a-4195-a319-e4e78827b1fd
# ╟─ee2f358b-f4da-47cb-bcc9-576d6126496f
# ╟─6a73f65c-15dc-4db8-9de2-c69cebe79070
# ╠═4789b052-974d-47a0-bcc8-9d327f2036d3
# ╠═c39b048b-3ede-432e-b9bd-7286cfcfd24a
# ╠═ae08f2c9-dff8-4cac-a4d7-cafc557e4d48
# ╠═b2bdee21-248d-4c1f-b211-9aaf28fc727a
# ╠═d4999823-4efe-4719-a47c-f7e306c3a1f2
# ╠═92df7c73-5aed-4102-a58a-daeb4759648c
# ╠═620a62be-337b-4c60-af38-9105e0d383ff
# ╟─6f9a6971-825c-48ef-b5bb-16afffdc9ef3
# ╠═55824f36-bde6-4359-b18e-9b23761f35cf
# ╠═3b1084c0-862d-4b27-89b2-fa8abed05e06
# ╟─3a4cc2b2-ee6a-497c-a060-7d10c13a3edb
# ╠═4995982d-90b5-4fe9-862b-4b5ba88b23b8
# ╠═01a5b902-6b4b-48a3-91be-dcef8067d5c2
# ╟─c0e187a9-c5e5-4538-8438-c140c6677724
# ╠═7710f8cb-c71d-4ac6-b755-39c9c270ba8d
# ╠═2fae4b02-57fb-44d9-ad3b-e3aaede3632e
