### A Pluto.jl notebook ###
# v0.19.40

using Markdown
using InteractiveUtils

# ╔═╡ 4acaacbd-196e-428b-b508-0257c2e82bca
# ╠═╡ show_logs = false
import Pkg; Pkg.activate(@__DIR__); Pkg.instantiate()

# ╔═╡ 3f7de967-4f0c-43de-b7d3-b3fa32548079
using CacheVariables, CairoMakie, Dictionaries, ProgressLogging, StableRNGs, StatsBase

# ╔═╡ 4be00396-e67f-4548-b4a6-817c2b0e372b
using LinearAlgebra

# ╔═╡ 4c79997b-9b38-4dcc-a13f-7d8446497a75
using DataFrames, DelimitedFiles, FITSIO, HTTP, Interpolations, Printf, StructArrays

# ╔═╡ 9b4bb7be-252b-494f-b979-1789941a83bb
md"""
# Figures 9-10
"""

# ╔═╡ d375790f-7c39-403c-b125-ba01454b0f7b
CACHEDIR = "$(splitext(@__FILE__)[1])-cache"

# ╔═╡ 017e7c78-9780-46e9-965b-a58b115a23cb
DATADIR = joinpath(CACHEDIR, "data")

# ╔═╡ 45c321dd-ab86-4304-884f-4a2eeacf1eee
# Load TerminalLoggers if running outside of Pluto
if !(@isdefined PlutoRunner)
	import Logging, TerminalLoggers
	Logging.global_logger(TerminalLoggers.TerminalLogger())
end

# ╔═╡ b93ed70d-ea76-4009-9b1f-821a45353c6a
md"""
## Load and preprocess data
"""

# ╔═╡ 5d7ad9fb-93ed-45fd-9030-91fc77ba0bf9
function cacheurl(url; dir="data", update_period=1)
	ispath(dir) || mkpath(dir)

	path = joinpath(dir, basename(url))
	ispath(path) || try
		HTTP.download(url, path; update_period)
	catch e
		ispath(path) && rm(path)
		throw(e)
	end

	return path
end

# ╔═╡ 42131c56-cc62-40e9-ada3-21a56cd66853
function loadspec(SURVEY, RUN2D, PLATE, MJD, FIBERID; dir=joinpath("data", "specs"))
	url = string(
		"https://dr16.sdss.org/sas/dr16/",
		SURVEY,
		"/spectro/redux/",
		RUN2D,
		"/spectra/lite/",
		@sprintf("%04d", PLATE),
		"/spec-",
		@sprintf("%04d", PLATE),
		"-",
		MJD,
		"-",
		@sprintf("%04d", FIBERID),
		".fits"
	)
	return FITS(f -> DataFrame(f[2]), cacheurl(url; dir, update_period=Inf))
end

# ╔═╡ c048c8f5-b122-4fe6-aa4d-f46c4a5937fb
WAVE = 1280:0.5:1820

# ╔═╡ f261b1ec-d1e4-47ec-ab26-edf82c743693
Y, V = cache(joinpath(CACHEDIR, "data.bson")) do
	# Load DR16Q and form list of spectra to load
	@info "Load DR16Q and form list of spectra to load"
	DR16Q_URL = "https://data.sdss.org/sas/dr16/eboss/qso/DR16Q/DR16Q_v4.fits"
	PLATE_URL = "https://data.sdss.org/sas/dr16/sdss/spectro/redux/plates-dr16.fits"
	dr16qp = innerjoin(
		FITS(f -> DataFrame(f[2]), cacheurl(DR16Q_URL; dir=DATADIR)),
		FITS(f -> DataFrame(f[2]), cacheurl(PLATE_URL; dir=DATADIR));
		on=[:PLATE,:MJD], validate=(false,true), makeunique=true
	)
	list = filter(dr16qp) do spec
		spec.SURVEY == "eboss" && spec.PLATEQUALITY == "good" &&
		2.0 < spec.Z < 2.1 && spec.BAL_PROB < 0.2
	end

	# Load spectra and filter based on wavelengths covered
	@info "Load spectra and filter based on wavelengths covered"
	specs = @withprogress name="Loading spectra" map(
		1:nrow(list),
		list.SURVEY, list.RUN2D, list.PLATE, list.MJD, list.FIBERID, list.Z
	) do idx, SURVEY, RUN2D, PLATE, MJD, FIBERID, Z
		df = loadspec(SURVEY,RUN2D,PLATE,MJD,FIBERID; dir=joinpath(DATADIR, "specs"))
		spec = (;
			WAVE = 10.0 .^ df.LOGLAM ./ (1+Z),   # Restframe wavelength
			FLUX = df.FLUX,                      # Flux
			IVAR = df.IVAR,                      # Inverse variance
		)
		@logprogress idx/nrow(list)
		return spec
	end
	specs = filter(specs) do spec
		first = findlast(<(minimum(WAVE)),spec.WAVE)
		last = findfirst(>(maximum(WAVE)),spec.WAVE)
		return !isnothing(first) && !isnothing(last) &&  # spec.WAVE covers WAVE
			!any(iszero,spec.IVAR[first:last])           # without missing entries
	end

	# Interpolate, center, and normalize spectra and variance profiles
	@info "Interpolate, center, and normalize spectra and variance profiles"
	normspecs = (StructArray∘map)(specs) do spec
		# Form data vectors via linear interpolation
		FLUX = LinearInterpolation(spec.WAVE, spec.FLUX).(WAVE)
		VAR  = LinearInterpolation(spec.WAVE, inv.(spec.IVAR)).(WAVE)

		# Center
		FLUX = FLUX .- mean(FLUX)

		# Normalize
		scaling = abs(mean(FLUX[1525 .< WAVE .< 1575]))
		FLUX = FLUX / scaling
		VAR  = VAR  / scaling^2

		return (;FLUX, VAR)
	end

	# Sort the spectra and select 5000 noisiest after dropping the 80 noisiest
	normspecs = sort(normspecs; by = spec->mean(spec.VAR))
	normspecs = normspecs[1:end-80]        # drop 80 noisiest
	normspecs = normspecs[end-5000+1:end]  # select 5000 noisiest (among remaining)

	# Form data matrix and variance profile
	@info "Form data matrix and variance profile"
	data = stack(normspecs.FLUX; dims=1)
	prof = stack(normspecs.VAR;  dims=1)

	return data, prof
end

# ╔═╡ fd027916-e827-4649-b58d-cf1ed365b246
n, p = size(Y)

# ╔═╡ 8d81ef65-3af3-4cd3-bf90-1b49521b5cc6
md"""
## Figure 9a
"""

# ╔═╡ 6f545908-3500-46a7-8902-42930126fc55
figure_09a = with_theme(; fontsize=18f0) do
	fig = Figure(; size=(500,300))

	# Visualize data
	axY = Axis(fig[2,1]; yreversed=true,
		xlabel="Rest frame wavelength", ylabel="Spectrum index")
	hmY = heatmap!(axY, WAVE, 1:n, permutedims(Y);
		colormap=:vik, colorrange=(-1,1).*3.25)
	Colorbar(fig[1,1], hmY; vertical=false, label="Data")

	# Visualize variance profile
	axV = Axis(fig[2,2]; yreversed=true,
		xlabel="Rest frame wavelength", ylabel="Spectrum index")
	hmV = heatmap!(axV, WAVE, 1:n, permutedims(V);
		colormap=:viridis, colorrange=(0,1))
	Colorbar(fig[1,2], hmV; vertical=false, label="Variance profile")

	# Link axes
	axY.yticks = axV.yticks = [1; 1000:1000:5000]
	linkaxes!(axY, axV)
	hideydecorations!(axV; ticks=false)

	# Layout
	colgap!(fig.layout, 30)

	fig
end

# ╔═╡ 23f8d87f-1e71-404f-ba3b-ed802dc00a67
save("figure-09a.png", figure_09a)

# ╔═╡ c804d4c7-a382-45b0-ba72-ebaf4f78e12d
md"""
## Figure 9b
"""

# ╔═╡ f9914e1c-8624-46f2-b06e-2fae46be5dbe
figure_09b = with_theme(; fontsize=18f0) do
	fig = Figure(; size=(400,250))
	ax = Axis(fig[1,1]; ylabel="singular values")
	scatter!(ax, svdvals(Y); markersize=7.5)
	ax.limits = ((0, 30), (0, 1250))
	ax.xticks = [1; 5:5:30]
	ax.yticks = 0:200:1200
	fig
end

# ╔═╡ 22e24487-0682-4427-868d-26d499fd3bcc
save("figure-09b.png", figure_09b)

# ╔═╡ caaf21a6-a1da-469b-bb8c-977bd59c89c9
md"""
## Figure 10
"""

# ╔═╡ 0914e551-dfb0-4439-a9e7-ae0f0d62fa0f
figure_10 = with_theme(; fontsize=16f0, linewidth=1) do
	fig = Figure(; size=(800, 300))

	## Compute eigenspectra
	U = svd(Y).V

	## Plot eigenspectra
	for (k, uk) in enumerate(eachcol(U)[1:8])
		row, col = fldmod1(k, 4)

		# Plot curve
		ax = Axis(fig[row,col]; yticks=[0.0])
		flip = maximum(abs, filter(>(0), uk)) < maximum(abs, filter(<(0), uk))
		lines!(ax, WAVE, flip ? -uk : uk)
		tightlimits!(ax, Left(), Right())

		# Add label
		Label(fig[row,col], L"\mathbf{u}_{%$k}"; padding=(0, 10, 0, 5), fontsize=24f0,
			tellheight=false, tellwidth=false, halign=:right, valign=:top)
	end
	linkxaxes!(filter(b -> b isa Axis, contents(fig.layout))...)

	## Add axis labels
	Label(fig[end+1,:], "x-axis: rest frame wavelength, y-axis: component entries")

	fig
end

# ╔═╡ 6e264442-8467-4fa4-8af2-59c02fa6bacd
save("figure-10.png", figure_10)

# ╔═╡ 7eb990bc-7c5f-4d1c-8229-ea0189397b64
md"""
## Load and run methods
"""

# ╔═╡ 87ed029c-5830-451b-84a1-b2ae382810ab
import FlipPA

# ╔═╡ a706dfd1-7b75-4153-adc9-e49890789b86
module BCF include(joinpath(@__DIR__, "methods", "bcf.jl")) end

# ╔═╡ 64179d00-bb3f-4b5b-aafc-ce51d3daa139
module GIC include(joinpath(@__DIR__, "methods", "gic.jl")) end

# ╔═╡ 5b7c09d9-4995-4fbd-853f-b2cd9c7e004e
module ACT include(joinpath(@__DIR__, "methods", "act.jl")) end

# ╔═╡ 7f9709d9-33f5-43fb-a8e6-ece25045c6e7
module BEMA0 include(joinpath(@__DIR__, "methods", "bema0.jl")) end

# ╔═╡ 717872c6-ecba-46ed-be1d-2a386ad8b319
module BEMA include(joinpath(@__DIR__, "methods", "bema.jl")) end

# ╔═╡ 42a35003-24bc-469c-809d-ef40933a3f73
module Biwhitening include(joinpath(@__DIR__, "methods", "biwhitening.jl")) end

# ╔═╡ 5136029d-1d16-4d35-b0ec-74a591dc38b9
PAconfig = (; quantile=1.0, trials=100, threshold=0.0)

# ╔═╡ fba4c71a-064e-42ae-9a90-e325004c2f38
METHODS = dictionary([
	"PermPA (pairwise)"   => X -> FlipPA.permpa(X; rng=StableRNG(0), PAconfig...,
												comparison=FlipPA.Pairwise()),
	"PermPA (upper-edge)" => X -> FlipPA.permpa(X; rng=StableRNG(0), PAconfig...,
												comparison=FlipPA.UpperEdge()),
	"BCF"                 => X -> BCF.select(X),
	"GIC"                 => X -> GIC.select(X),
	"ACT (max rank=1000)" => X -> ACT.select(X; rmax=1000),
	"BEMA0"               => X -> BEMA0.select(X),
	"BEMA"                => X -> BEMA.select(X;
								seed=rand(StableRNG(0),zero(Int32):typemax(Int32))),
	"Biwhitening"         => X -> Biwhitening.select(X; maxiters=1000),
	"FlipPA (pairwise)"   => X -> FlipPA.flippa(X; rng=StableRNG(0), PAconfig...,
												comparison=FlipPA.Pairwise()),
	"FlipPA (upper-edge)" => X -> FlipPA.flippa(X; rng=StableRNG(0), PAconfig...,
												comparison=FlipPA.UpperEdge()),
])

# ╔═╡ 77330d0b-0cb8-478d-b159-e9a7e83aef89
map(pairs(METHODS)) do (method, method_func)
	smethod = replace(method, "("=>"", ")"=>"", "="=>" ")
	@cache joinpath(CACHEDIR, "estimates", "$smethod.bson") method_func(Y)
end

# ╔═╡ 6a73f65c-15dc-4db8-9de2-c69cebe79070


# ╔═╡ Cell order:
# ╟─9b4bb7be-252b-494f-b979-1789941a83bb
# ╠═4acaacbd-196e-428b-b508-0257c2e82bca
# ╠═3f7de967-4f0c-43de-b7d3-b3fa32548079
# ╠═4be00396-e67f-4548-b4a6-817c2b0e372b
# ╠═d375790f-7c39-403c-b125-ba01454b0f7b
# ╠═017e7c78-9780-46e9-965b-a58b115a23cb
# ╠═45c321dd-ab86-4304-884f-4a2eeacf1eee
# ╟─b93ed70d-ea76-4009-9b1f-821a45353c6a
# ╠═4c79997b-9b38-4dcc-a13f-7d8446497a75
# ╟─5d7ad9fb-93ed-45fd-9030-91fc77ba0bf9
# ╟─42131c56-cc62-40e9-ada3-21a56cd66853
# ╠═c048c8f5-b122-4fe6-aa4d-f46c4a5937fb
# ╠═f261b1ec-d1e4-47ec-ab26-edf82c743693
# ╠═fd027916-e827-4649-b58d-cf1ed365b246
# ╟─8d81ef65-3af3-4cd3-bf90-1b49521b5cc6
# ╠═6f545908-3500-46a7-8902-42930126fc55
# ╠═23f8d87f-1e71-404f-ba3b-ed802dc00a67
# ╟─c804d4c7-a382-45b0-ba72-ebaf4f78e12d
# ╠═f9914e1c-8624-46f2-b06e-2fae46be5dbe
# ╠═22e24487-0682-4427-868d-26d499fd3bcc
# ╟─caaf21a6-a1da-469b-bb8c-977bd59c89c9
# ╠═0914e551-dfb0-4439-a9e7-ae0f0d62fa0f
# ╠═6e264442-8467-4fa4-8af2-59c02fa6bacd
# ╟─7eb990bc-7c5f-4d1c-8229-ea0189397b64
# ╠═87ed029c-5830-451b-84a1-b2ae382810ab
# ╠═a706dfd1-7b75-4153-adc9-e49890789b86
# ╠═64179d00-bb3f-4b5b-aafc-ce51d3daa139
# ╠═5b7c09d9-4995-4fbd-853f-b2cd9c7e004e
# ╠═7f9709d9-33f5-43fb-a8e6-ece25045c6e7
# ╠═717872c6-ecba-46ed-be1d-2a386ad8b319
# ╠═42a35003-24bc-469c-809d-ef40933a3f73
# ╠═5136029d-1d16-4d35-b0ec-74a591dc38b9
# ╠═fba4c71a-064e-42ae-9a90-e325004c2f38
# ╠═77330d0b-0cb8-478d-b159-e9a7e83aef89
# ╠═6a73f65c-15dc-4db8-9de2-c69cebe79070
