## Setup script
# Installs and configures needed Julia and R packages

using Logging, Pkg

# Activate environment
Pkg.activate(@__DIR__)

# Install Julia packages (skip precompilation for RCall)
@info "Installing Julia packages..."
ENV["JULIA_PKG_PRECOMPILE_AUTO"] = 0
Pkg.instantiate()
delete!(ENV, "JULIA_PKG_PRECOMPILE_AUTO")

# Install R packages (using CondaPkg.jl)
@info "Installing R packages via CondaPkg into local environment..."
using CondaPkg
CondaPkg.resolve()

# Setup RCall to use CondaPkg environment
@info "Connecting Julia and R..."
using Libdl, Preferences, UUIDs
const RCALL_UUID = UUID("6f49c342-dc21-5d91-9882-a32aef131414")
RHOME = joinpath(CondaPkg.envdir(), "lib", "R")
if Sys.iswindows()
    LIBR = joinpath(RHOME, "bin", Sys.WORD_SIZE == 64 ? "x64" : "i386", "R.dll")
else
    LIBR = joinpath(RHOME, "lib", "libR.$(Libdl.dlext)")
end
set_preferences!(RCALL_UUID, "Rhome" => RHOME, "libR" => LIBR)

# Trigger precompilation
Pkg.precompile()

# Done!
@info "Setup done!"
