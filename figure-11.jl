### A Pluto.jl notebook ###
# v0.19.40

using Markdown
using InteractiveUtils

# ╔═╡ 4acaacbd-196e-428b-b508-0257c2e82bca
# ╠═╡ show_logs = false
import Pkg; Pkg.activate(@__DIR__); Pkg.instantiate()

# ╔═╡ 3f7de967-4f0c-43de-b7d3-b3fa32548079
using CacheVariables, CairoMakie, Dictionaries, ProgressLogging, StableRNGs, StatsBase

# ╔═╡ 4be00396-e67f-4548-b4a6-817c2b0e372b
using LinearAlgebra

# ╔═╡ 9b4bb7be-252b-494f-b979-1789941a83bb
md"""
# Figure 11
"""

# ╔═╡ 2da4d2fa-efaf-4231-bcab-8d2b5d7455db
# Get subset of sweep to run from environment variable (useful when parallelizing)
SWEEP_SUBSET = haskey(ENV, "SLURM_ARRAY_TASK_ID") ?
	(IDX = parse(Int, ENV["SLURM_ARRAY_TASK_ID"]); IDX:IDX) : Colon()

# ╔═╡ 52722187-04f4-4ec3-ae93-70d9ee377db2
# Load TerminalLoggers if running outside of Pluto
if !(@isdefined PlutoRunner)
	import Logging, TerminalLoggers
	Logging.global_logger(TerminalLoggers.TerminalLogger())
end

# ╔═╡ 7eb990bc-7c5f-4d1c-8229-ea0189397b64
md"""
## Load methods
"""

# ╔═╡ 87ed029c-5830-451b-84a1-b2ae382810ab
import FlipPA

# ╔═╡ a706dfd1-7b75-4153-adc9-e49890789b86
module BCF include(joinpath(@__DIR__, "methods", "bcf.jl")) end

# ╔═╡ 64179d00-bb3f-4b5b-aafc-ce51d3daa139
module GIC include(joinpath(@__DIR__, "methods", "gic.jl")) end

# ╔═╡ 5b7c09d9-4995-4fbd-853f-b2cd9c7e004e
module ACT include(joinpath(@__DIR__, "methods", "act.jl")) end

# ╔═╡ 7f9709d9-33f5-43fb-a8e6-ece25045c6e7
module BEMA0 include(joinpath(@__DIR__, "methods", "bema0.jl")) end

# ╔═╡ 717872c6-ecba-46ed-be1d-2a386ad8b319
module BEMA include(joinpath(@__DIR__, "methods", "bema.jl")) end

# ╔═╡ 42a35003-24bc-469c-809d-ef40933a3f73
module Biwhitening include(joinpath(@__DIR__, "methods", "biwhitening.jl")) end

# ╔═╡ 5136029d-1d16-4d35-b0ec-74a591dc38b9
PAconfig = (; quantile=1.0, trials=100, threshold=0.0)

# ╔═╡ fba4c71a-064e-42ae-9a90-e325004c2f38
METHODS = dictionary([
	"PermPA"      => (X, rng) -> FlipPA.permpa(X; rng, PAconfig...),
	"BCF"         => (X, rng) -> BCF.select(X),
	"GIC"         => (X, rng) -> GIC.select(X),
	"ACT"         => (X, rng) -> ACT.select(X; rmax=100),
	"BEMA0"       => (X, rng) -> BEMA0.select(X),
	# "BEMA"        => (X, rng) -> BEMA.select(X;
	# 								seed=rand(rng,zero(Int32):typemax(Int32))),
	"Biwhitening" => (X, rng) -> Biwhitening.select(X; maxiters=1000),
	"FlipPA"      => (X, rng) -> FlipPA.flippa(X; rng, PAconfig...),
	"FlipPA (pairwise)" =>
		(X, rng) -> FlipPA.flippa(X; rng, PAconfig..., comparison=FlipPA.Pairwise()),
])

# ╔═╡ 6a73f65c-15dc-4db8-9de2-c69cebe79070
md"""
## Run simulation
"""

# ╔═╡ 4789b052-974d-47a0-bcc8-9d327f2036d3
n, p = 60, 5000

# ╔═╡ c39b048b-3ede-432e-b9bd-7286cfcfd24a
θrange = (0:0.5:8.0)[SWEEP_SUBSET]

# ╔═╡ 6e4f941e-b674-4b22-9b13-f66b860e97a2
v = 1

# ╔═╡ 1e2e9325-9a6d-4290-a296-78e60fb11cba
runs = 100

# ╔═╡ 7214de72-52fd-4afd-aa9b-a342d5b8a02c
results = map(pairs(METHODS)) do (method, method_func)
	@withprogress name=method map(enumerate(θrange)) do (θidx, θ)
		@logprogress (θidx-1)/length(θrange)
		smethod = replace(method, "("=>"", ")"=>"")
		CACHE = joinpath("$(splitext(@__FILE__)[1])-cache", smethod, "theta-$θ.bson")
		@cache CACHE @withprogress name="$method (θ=$θ)" (countmap∘map)(1:runs) do run
			@logprogress (run-1)/runs
			rng = StableRNG(run)

			# Generate data
			u, z = normalize(randn(rng,n)), normalize(randn(rng,p))
			N = randn(rng,n,p) .* sqrt(v/n)
			X = θ*u*z' + N

			# Run method
			return method_func(X,rng)
		end
	end
end

# ╔═╡ 51a87aa8-905f-4485-9674-e7b2e67e8246
md"""
## Plot results
"""

# ╔═╡ aeba5491-1c0c-4871-906b-41387398afa6
countmean(dict::Dict) = sum(prod,dict)/sum(last,dict)

# ╔═╡ 521e75e8-9580-4075-903b-b285cf8ca008
predprob(pred,dict::Dict) =
	sum([cnt for (val,cnt) in pairs(dict) if pred(val)]) / sum(last,dict)

# ╔═╡ cf68bbbf-d3ab-49f4-8bd3-e442ea6ad822
PALETTE = to_colormap(:Set2_8)[[1,6,3,4,5,2,7,8]]

# ╔═╡ d920319f-6f11-4f73-aad1-be989d7dffc2
PLOT_LIST = [
	"PermPA"       => (; color = PALETTE[1]),
	"BCF"          => (; color = PALETTE[2]),
	"GIC"          => (; color = PALETTE[3]),
	"ACT"          => (; color = PALETTE[4], linestyle = :dash),
	"BEMA0"        => (; color = PALETTE[5]),
	# "BEMA"         => (; color = PALETTE[6]),
	"Biwhitening"  => (; color = PALETTE[7]),
	"FlipPA"       => (; color = :black, linestyle = :dashdot),
	# "FlipPA (pairwise)" => (; color = :red),
]

# ╔═╡ 43e2e703-4206-4582-8f0b-02b7f7d3395c
figure_11 = with_theme(; linewidth = 2) do
	# Setup
	ktarget = 1  # target rank
	preds = [    # probability predicates
		>(ktarget)  => L"\mathrm{Pr}[\hat{k} > %$(ktarget)]",
		==(ktarget) => L"\mathrm{Pr}[\hat{k} = %$(ktarget)]",
		<(ktarget)  => L"\mathrm{Pr}[\hat{k} < %$(ktarget)]",
	]

	# Make figure
	fig = Figure()

	## Probability curves
	for (row, (pred, ylabel)) in enumerate(preds)
		ax = Axis(fig[row, 2])
		for (method, plot_args) in PLOT_LIST
			lines!(ax, θrange, predprob.(pred, results[method]); plot_args...)
		end
		tightlimits!(ax, Left(), Right())
		ylims!(ax, -0.05, 1.05)
		ax.xlabel = L"\theta"
		ax.ylabel = ylabel
		row == length(preds) || hidexdecorations!(ax; grid=false, ticks=false)
	end

	## Mean curves w/ broken axes
	BAxis = (;
		ylims      = [(58.7, 60.2) => 59:60, (0, 1.3) => 0:1],
		gap        = 10,
		lineangle  = pi/8,
		linelength = 20,
		linewidth  = 1,
	)
	BAxis_grid = GridLayout(fig[:, 1]; default_rowgap = BAxis.gap)
	Label(fig[:, 0], L"\mathrm{E}[\hat{k}]";
		rotation=pi/2, tellheight=false, tellwidth=true)
	for (row, (ylims, yticks)) in enumerate(BAxis.ylims)
		ax = Axis(BAxis_grid[row, 1])
		for (method, plot_args) in PLOT_LIST
			lines!(ax, θrange, countmean.(results[method]);
				label=method, plot_args...)
		end
		tightlimits!(ax, Left(), Right())
		ylims!(ax, ylims)
		ax.yticks = yticks
		ax.xlabel = L"\theta"
		rowsize!(BAxis_grid, row, Auto(ylims[2] - ylims[1]))
		row == length(BAxis.ylims) || hidexdecorations!(ax; grid=false)
		row == length(BAxis.ylims) || hidespines!(ax, :b)
		row == 1                   || hidespines!(ax, :t)
	end
	linkxaxes!(contents(BAxis_grid)...)
	BAxis_lines = lift(
		content(BAxis_grid[1,1]).xaxis.attributes.endpoints,
		[ax.yaxis.attributes.endpoints for ax in contents(BAxis_grid)]...
	) do xendpoint, yendpoints...
		xcoords = getindex.(xendpoint, 1)
		ycoords = (Tuple∘Iterators.flatten)(
			getindex.(points, 2) for points in reverse(yendpoints)
		)[begin+1:end-1]
		v = Point2f(cos(BAxis.lineangle), sin(BAxis.lineangle)) * BAxis.linelength/2
		map(p -> (p + v, p - v), Point2f(x, y) for x in xcoords for y in ycoords)
	end
	linesegments!(fig.scene, BAxis_lines; color=:black, linewidth=BAxis.linewidth)
	ax = content(BAxis_grid[1,1])

	## Legend
	Legend(fig[:,3], ax, "Methods", framevisible=false, rowgap=0)

	## Layout
	rowsize!(fig.layout, 1, Fixed(40))
	rowsize!(fig.layout, 2, Fixed(40))
	rowsize!(fig.layout, 3, Fixed(40))
	colsize!(fig.layout, 1, Fixed(200))
	colsize!(fig.layout, 2, Fixed(200))
	resize_to_layout!(fig)

	fig
end

# ╔═╡ cf53d538-5d41-48e5-a357-6ce73d915659
if SWEEP_SUBSET isa Colon    # save if we have the full sweep
	save("figure-11.png", figure_11; px_per_unit=4)
end

# ╔═╡ 92cb6157-434b-42e6-a3c4-3ebe33949867
md"""
## Calculate Type I error
"""

# ╔═╡ 7b0e4cc1-3bd8-4807-b763-18ca36699bd2
if any(iszero, θrange)
	map(results) do result
		nullresult = only(result[θrange .== 0])
		nruns = sum([cnt for (val,cnt) in pairs(nullresult) if val > 0])
		rate  = predprob(>(0), nullresult)
		"$nruns/$runs runs = $rate"
	end
else
	"θrange doesn't include θ=0 case"
end

# ╔═╡ Cell order:
# ╟─9b4bb7be-252b-494f-b979-1789941a83bb
# ╠═4acaacbd-196e-428b-b508-0257c2e82bca
# ╠═3f7de967-4f0c-43de-b7d3-b3fa32548079
# ╠═4be00396-e67f-4548-b4a6-817c2b0e372b
# ╠═2da4d2fa-efaf-4231-bcab-8d2b5d7455db
# ╠═52722187-04f4-4ec3-ae93-70d9ee377db2
# ╟─7eb990bc-7c5f-4d1c-8229-ea0189397b64
# ╠═87ed029c-5830-451b-84a1-b2ae382810ab
# ╠═a706dfd1-7b75-4153-adc9-e49890789b86
# ╠═64179d00-bb3f-4b5b-aafc-ce51d3daa139
# ╠═5b7c09d9-4995-4fbd-853f-b2cd9c7e004e
# ╠═7f9709d9-33f5-43fb-a8e6-ece25045c6e7
# ╠═717872c6-ecba-46ed-be1d-2a386ad8b319
# ╠═42a35003-24bc-469c-809d-ef40933a3f73
# ╠═5136029d-1d16-4d35-b0ec-74a591dc38b9
# ╠═fba4c71a-064e-42ae-9a90-e325004c2f38
# ╟─6a73f65c-15dc-4db8-9de2-c69cebe79070
# ╠═4789b052-974d-47a0-bcc8-9d327f2036d3
# ╠═c39b048b-3ede-432e-b9bd-7286cfcfd24a
# ╠═6e4f941e-b674-4b22-9b13-f66b860e97a2
# ╠═1e2e9325-9a6d-4290-a296-78e60fb11cba
# ╠═7214de72-52fd-4afd-aa9b-a342d5b8a02c
# ╟─51a87aa8-905f-4485-9674-e7b2e67e8246
# ╠═aeba5491-1c0c-4871-906b-41387398afa6
# ╠═521e75e8-9580-4075-903b-b285cf8ca008
# ╠═cf68bbbf-d3ab-49f4-8bd3-e442ea6ad822
# ╠═d920319f-6f11-4f73-aad1-be989d7dffc2
# ╠═43e2e703-4206-4582-8f0b-02b7f7d3395c
# ╠═cf53d538-5d41-48e5-a357-6ce73d915659
# ╟─92cb6157-434b-42e6-a3c4-3ebe33949867
# ╠═7b0e4cc1-3bd8-4807-b763-18ca36699bd2
