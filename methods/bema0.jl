## BEMA0

using RCall

function select(X; alpha=0.1)
    _ensure_BEMA0_defined()

    # based on https://github.com/ZhengTracyKe/BEMA/blob/f02166c7e0e29d03a499aae7ad3a9f11e54a5dad/BEMA0.R#L22-L29
    return R"""
n=$(size(X,1))
p=$(size(X,2))
data=$(X)
SQM($(alpha))
    """ |> rcopy
end

function _ensure_BEMA0_defined()
    if rcopy(R"exists(\"SQM\")")
        return
    end

    # copied from https://github.com/ZhengTracyKe/BEMA/blob/f02166c7e0e29d03a499aae7ad3a9f11e54a5dad/BEMA0.R#L1-L20
    # (with lines for plotting and unused package commented out)
    R"""
library(MASS)
library(RMTstat)
# library(rmatio)
library(pracma)

SQM <- function(alpha){
  S <- t(data) %*% data  /n
  l <- eigen(S)$values
  gamma=min(p,n)/max(p,n)
  k=floor(min(p,n)*alpha):floor(min(p,n)*(1-alpha))
  predictor=qmp(k/min(p,n),max(n,p),min(n,p))*max(p,n)/n
  
  sigma2hat=lm(rev(l[k])~predictor-1)$coef[[1]]
#   plot(l)
#   k0=min(p,n):1
#   lines(qmp(k0/min(p,n),max(n,p),min(n,p))*max(p,n)/n*sigma2hat,col='red')
  cutoff=(sigma2hat*((1+sqrt(gamma))^2+qtw(0.9)*max(p,n)^(-2/3)*gamma^(-1/6)*(1+sqrt(gamma))^(4/3)))*max(p,n)/n
  K=sum(l>cutoff)
  return(K)
}
    """
    return
end
