## Biwhitening

using LinearAlgebra

function select(X; δ=0.0, maxiters)
    n, p = size(X)
    w1, w2 = sinkhornknopp(X.^2,p*ones(n),n*ones(p),δ,maxiters)
    Xbw = sqrt(Diagonal(w1))*X*sqrt(Diagonal(w2))
    return count(>(sqrt(n)+sqrt(p)),svdvals(Xbw))
end

function sinkhornknopp(A,r,c,δ,maxiters)
    # Checks
    all(>=(zero(eltype(A))), A) ||
        throw(DomainError(A, "`A` must have nonnegative entries."))
    all(>=(zero(eltype(r))), r) ||
        throw(DomainError(r, "`r` must have nonnegative entries."))
    all(>=(zero(eltype(c))), c) ||
        throw(DomainError(c, "`c` must have nonnegative entries."))
    δ >= zero(δ) ||
        throw(DomainError(δ, "`δ` must be nonnegative."))
    
    # 0: Setup
    m, n = size(A)
    μ = sum(c)/sum(r)

    # 1: Initialize
    x = ones(m)
    y = ones(n)
    τ = 0.0

    # 2: iterate
    for τ in 1:maxiters
        if !(
            maximum(abs(sum(x[i]*A[i,j]*y[j] for j in 1:n) - μ*r[i]) for i in 1:m) > δ ||
            maximum(abs(sum(x[i]*A[i,j]*y[j] for i in 1:m) -   c[j]) for j in 1:n) > δ
        )
            break
        end

        for j in 1:n
            y[j] = c[j]/sum(A[i,j]*x[i] for i in 1:m)
        end
        for i in 1:m
            x[i] = μ*r[i]/sum(A[i,j]*y[j] for j in 1:n)
        end
    end

    return x, y
end
