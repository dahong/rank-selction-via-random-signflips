## GIC

using LinearAlgebra

function select(X; krange=0:(size(X,2)-1))
    n, p = size(X)

    # Compute eigenvalues
    dnz = (svdvals(X).^2)./n
    d = [dnz; zeros(eltype(dnz),p-length(dnz))]

    # Compute default gamma
    c = p/n
    φc = 1/2+sqrt(1/c)-log(1+sqrt(c))/c
    γ = min(1.1*φc,1)

    # Minimize GIC
    return argmin(krange) do k
        db_kp1 = sum([d[i]/(p-k) for i in k+1:p])
        n/2*(sum([log(d[i]) for i in 1:k]) + (p-k)*log(db_kp1)) + γ*k*(p-k/2+1/2)
    end
end
