## ACT

using RCall

# extracted from implemenation in `CodeData.zip/simuexam/Simulation.R`
function select(X; rmax)
    # setup (based on lines 3,4,7,84)
    R"""
pZ<-$(size(X,2))
n<-$(size(X,1))
rmax<-$(rmax)
Data<-$(permutedims(X))
    """

    # run method (copied from lines 85-99)
    R"""
  X=Data-matrix(rep(t(apply(Data,1,mean)),n),ncol=n)

  ## Method 1: the method of zheng: denote0[,13]
  sn<-cov(t(X)); hatRR=cov2cor(sn); lambdaZ<-eigen(hatRR)$values; DD=NULL; lambdaLY=lambdaZ; p=pZ
  pp=rmax+2; mz=rep(0,pp); dmz=mz; tmz=mz
  
      for (kk in 1:pp){qu=3/4
      lambdaZ1=lambdaZ[-seq(max(0, 1),kk,1)]; z0=qu*lambdaZ[kk]+(1-qu)*lambdaZ[kk+1]
      ttt=c(lambdaZ1-lambdaZ[kk], z0-lambdaZ[kk]); 
      y0=(length(lambdaZ1))/(n-1)
      mz[kk]=-(1-y0)/lambdaZ[kk]+y0*mean(na.omit(1/ttt))
             }
  
  temp2018=(-1/mz)[-1]-1-sqrt(pZ/(n-1)); temp1=seq(1,rmax,1); temp2=cbind(temp1, temp2018[1:rmax])
  k00new=max((temp2[,1][temp2[,2]>0]), 0)+1
    """

    # output
    return rcopy(Int,R"k00new")
end
