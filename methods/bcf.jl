## BCF

using LinearAlgebra

function select(X; krange=0:(size(X,2)-1))
    n, p = size(X)

    # Compute eigenvalues
    dnz = (svdvals(X).^2)./n
    d = [dnz; zeros(eltype(dnz),p-length(dnz))]

    # Minimize appropriate criterion
    c = p/n
    if 0 < c < 1
        return argmin(krange) do k
            db_kp1 = sum([d[i]/(p-k) for i in k+1:p])
            -sum([log(d[i]) for i in k+1:p]) + (p-k)*log(db_kp1) - (p-k-1)*(p-k+2)/n
        end
    elseif c > 1
        return argmin(krange) do k
            db_kp1 = sum([d[i]/(n-1-k) for i in k+1:n-1])
            -sum([log(d[i]) for i in k+1:n-1]) + (n-1-k)*log(db_kp1) - (n-k-2)*(n-k+1)/p
        end
    else
        throw("unspecified case")
    end
end
