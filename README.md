# Selecting the number of components in PCA via random signflips

Repository containing code associated with the paper

> David Hong, Yue Sheng, Edgar Dobriban. "Selecting the number of components in PCA via random signflips", 2024. https://arxiv.org/abs/2012.02985v3.

## Installation

The code was run on Julia 1.10.2,
which can be installed using the Juliaup installation manager:
https://julialang.org/downloads/

To install the relevant Julia and R packages,
run the `setup.jl` script from this directory:

```bash
$ julia setup.jl
```

This script installs and sets up a reproducible environment for the code:

+ **Installs the Julia packages in this environment**

    The environment is defined by the `JuliaProject.toml` and `JuliaManifest.toml` files,
    which specify the packages used
    (including their versions as well as the versions of all their dependencies).

    For more info see: https://pkgdocs.julialang.org/v1/environments/#Using-someone-else's-project

+ **Installs R and the R packages in this environment using CondaPkg**

    The R packages needed are defined by the `CondaPkg.toml` file,
    which specifies the R packages used (including their versions).
    This step installs a copy of R and the relevant R packages
    specific to this environment
    using the CondaPkg package (https://github.com/JuliaPy/CondaPkg.jl).

    This step creates a `.CondaPkg/` directory containing the Conda files.

+ **Sets up Julia to run R code**

    The Julia code runs methods implemented in R by calling in to R
    using the RCall package (https://github.com/JuliaInterop/RCall.jl).
    This step configures RCall to use the environment-specific copy of R
    and creates a corresponding `LocalPreferences.toml` file.

    For more info see: https://juliainterop.github.io/RCall.jl/dev/installation/#(Experimental)-Usage-with-CondaPkg

## `figure-01-03.jl`

This script creates Figures 1-3 from the paper.
It can be run as a script:
```bash
$ julia figure-01-03.jl
```
which produces the following images:

+ **`figure-01a.png`**

    ![](figure-01a.png)

+ **`figure-01b.png`**

    ![](figure-01b.png)

+ **`figure-02.png`**

    ![](figure-02.png)

+ **`figure-03.png`**

    ![](figure-03.png)

## `figure-04.jl`

This script creates Figure 4 from the paper.
It can be run as a script:
```bash
$ julia figure-04.jl
```
which produces the image **`figure-04.png`**:

![](figure-04.png)

## `figure-05.jl`

This script creates Figure 5 from the paper.
It can be run as a script:
```bash
$ julia figure-05.jl
```
which produces the image **`figure-05.png`**:

![](figure-05.png)

The script caches the results of long-running calculations/simulations
in the `figure-05-cache/` directory.

## `figure-06.jl`

This script creates Figure 6 from the paper.
It can be run as a script:
```bash
$ julia figure-06.jl
```
which produces the image **`figure-06.png`**:

![](figure-06.png)

The script caches the results of long-running calculations/simulations
in the `figure-06-cache/` directory.

## `figure-07.jl`

This script creates Figure 7 from the paper.
It can be run as a script:
```bash
$ julia figure-07.jl
```
which produces the image **`figure-07.png`**:

![](figure-07.png)

The script caches the results of long-running calculations/simulations
in the `figure-07-cache/` directory.

## `figure-08.jl`

This script creates Figure 8 from the paper.
It can be run as a script:
```bash
$ julia figure-08.jl
```
which produces the image **`figure-08.png`**:

![](figure-08.png)

The script caches the results of long-running calculations/simulations
in the `figure-08-cache/` directory.

## `figure-09-10.jl`

This script creates Figures 9-10 from the paper.
It can be run as a script:
```bash
$ julia figure-09-10.jl
```
which produces the following images:

+ **`figure-09a.png`**

    ![](figure-09a.png)

+ **`figure-09b.png`**

    ![](figure-09b.png)

+ **`figure-10.png`**

    ![](figure-10.png)

The script caches the results of long-running calculations/simulations
in the `figure-09-10-cache/` directory.

## `figure-11.jl`

This script creates Figure 11 from the paper.
It can be run as a script:
```bash
$ julia figure-11.jl
```
which produces the image **`figure-11.png`**:

![](figure-11.png)

The script caches the results of long-running calculations/simulations
in the `figure-11-cache/` directory.

## `figure-12.jl`

This script creates Figure 12 from the paper.
It can be run as a script:
```bash
$ julia figure-12.jl
```
which produces the image **`figure-12.png`**:

![](figure-12.png)

The script caches the results of long-running calculations/simulations
in the `figure-12-cache/` directory.

## `figure-13.jl`

This script creates Figure 13 from the paper.
It can be run as a script:
```bash
$ julia figure-13.jl
```
which produces the image **`figure-13.png`**:

![](figure-13.png)

The script caches the results of long-running calculations/simulations
in the `figure-13-cache/` directory.

## `figure-14.jl`

This script creates Figure 14 from the paper.
It can be run as a script:
```bash
$ julia figure-14.jl
```
which produces the image **`figure-14.png`**:

![](figure-14.png)

The script caches the results of long-running calculations/simulations
in the `figure-14-cache/` directory.

## Running the scripts as Pluto notebooks

The above scripts are actually Pluto notebooks (https://plutojl.org).
To run them as notebooks, launch julia in this directory
then run the following from the Julia REPL:
```julia-repl
julia> import Pkg; Pkg.activate(".")  # activate the project environment
julia> import Pluto; Pluto.run()      # launch Pluto
```
This will launch the notebook interface in your web browser.
Use the "Open a notebook" field to launch the desired script/notebook.
