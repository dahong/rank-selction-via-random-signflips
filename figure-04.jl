### A Pluto.jl notebook ###
# v0.19.40

using Markdown
using InteractiveUtils

# ╔═╡ 4acaacbd-196e-428b-b508-0257c2e82bca
# ╠═╡ show_logs = false
import Pkg; Pkg.activate(@__DIR__); Pkg.instantiate()

# ╔═╡ 3f7de967-4f0c-43de-b7d3-b3fa32548079
using CairoMakie, LinearAlgebra, Random, StableRNGs, Statistics

# ╔═╡ 9b4bb7be-252b-494f-b979-1789941a83bb
md"""
# Figure 4
"""

# ╔═╡ 7eb990bc-7c5f-4d1c-8229-ea0189397b64
md"""
## Utility functions
"""

# ╔═╡ 3577f9d8-0d3b-4f92-b3c9-561bd3b45aee
function Arrow(figpos, coords; linewidth=5, arrowsize=10, color=Cycled(1))
	# Create axis
	ax = Axis(figpos)

	# Shorten final line segment for arrow head (error if too short)
	delta = coords[end] .- coords[end-1]
	arrowsize < norm(delta)/2 ||
		error("Arrow head is too large, need `arrowsize < $(norm(delta)/2)`")
	scoords = [coords[1:end-1]; coords[end] .- arrowsize.*(delta./norm(delta))]

	# Draw arrow
	scatter!(ax, scoords[end]...; space=:pixel, markersize=2*arrowsize,
		marker=:utriangle, rotations=-atan(delta...), color)
	lines!(ax, scoords; space=:pixel, linewidth, color)

	# Hide axis and return
	hidedecorations!(ax)
	hidespines!(ax)
	return ax
end

# ╔═╡ 9bb27ce0-bf0a-4195-a319-e4e78827b1fd
function MatrixViz(figpos, matrix; unit_per_entry=2,
	title="", titlesize=16f0, titlepadding=5,
	rowlabel="", columnlabel="", labelsize=12f0, labelpadding=(4,4))

	# Plot heatmap
	grid = GridLayout(figpos)
	ax = Axis(grid[1,1]; yreversed=true, aspect=DataAspect())
	hm = heatmap!(ax, permutedims(matrix))
	rowsize!(grid, 1, Fixed(size(matrix,1)*unit_per_entry))
	colsize!(grid, 1, Fixed(size(matrix,2)*unit_per_entry))

	# Add title and labels
	if !isempty(title)
		Label(grid[1,1,Bottom()], title; valign=:top,
			padding=(0,0,0,titlepadding), fontsize=titlesize)
	end
	if !isempty(rowlabel)
		Label(grid[1,1,Left()], "⟵ $rowlabel"; valign=:top, rotation=pi/2,
			padding=(0,labelpadding[2],0,labelpadding[1]), fontsize=labelsize)
	end
	if !isempty(columnlabel)
		Label(grid[1,1,Top()], "$columnlabel ⟶"; halign=:left,
			padding=(labelpadding[1],0,labelpadding[2],0), fontsize=labelsize)
	end

	# Hide axis and return
	hidedecorations!(ax)
	hidespines!(ax)
	return grid, ax, hm
end

# ╔═╡ 6a73f65c-15dc-4db8-9de2-c69cebe79070
md"""
## Generate variance profiles
"""

# ╔═╡ 4789b052-974d-47a0-bcc8-9d327f2036d3
n, p = 500, 800

# ╔═╡ 8a8b3358-be9c-4c8a-baf7-6c14c444275a
V = [0.1*ones(n÷2, p); 0.9*ones(n÷2, p)];

# ╔═╡ f4af2183-3121-4bf4-8c6b-f6a980136870
v = mapslices(mean, V; dims=1);

# ╔═╡ 6f9a6971-825c-48ef-b5bb-16afffdc9ef3
md"""
## Create plot
"""

# ╔═╡ 55824f36-bde6-4359-b18e-9b23761f35cf
figure_04 = with_theme(; fontsize = 64f0,
	Heatmap = (; colormap=:vik, colorrange=(-0.1,0.1).*sqrt(n)),
	Hist    = (; bins=(0:0.05:3.5)*sqrt(n), normalization=:pdf, strokewidth=0.5),
	palette = (; patchcolor=Makie.wong_colors(0.6)),
) do
	fig = Figure()

	# Generate noise matrices
	rng = StableRNG(1)
	N  = randn(rng, n, p) .* sqrt.(V)
	Nπ = mapslices(x -> shuffle(rng, x), N; dims=1)
	Nb = randn(rng, n, p) .* sqrt.(v)

	# Visualize matrices
	MatrixViz(fig[1,1], N;  unit_per_entry=1, titlesize=64f0, labelsize=48f0,
		title="noise", rowlabel="observations", columnlabel="features")
	MatrixViz(fig[1,3], Nπ; unit_per_entry=1, titlesize=64f0, title="permuted noise")
	MatrixViz(fig[2,3], Nb; unit_per_entry=1, titlesize=64f0,
		title="noise with homogenized\nvariance profile")

	# Add arrow and "v.s." label
	ax = Arrow(fig[1,2], [(0, n÷2), (280, n÷2)]; color=:hotpink,
			linewidth=20, arrowsize=40)
	text!(ax, 140, n÷2; space=:pixel, align=(:center, :baseline), offset=(0, 40),
		text="permute", fontsize=64f0, color=:hotpink)
	colsize!(fig.layout, 2, Fixed(280))
	Label(fig[2,2], "v.s."; tellheight=false, fontsize=128f0)

	# Plot empirical singular value distributions
	ax = Axis(fig[:,4])
	hist!(ax, svdvals(N);  label="original noise")
	hist!(ax, svdvals(Nb); label="noise with homogenized\nvariance profile")
	hist!(ax, svdvals(Nπ); label="permuted noise")
	axislegend(ax; framevisible=false, margin=(0,40,0,40), rowgap=40,
		patchsize=(60,60), patchlabelgap=30)
	tightlimits!(ax, Left(), Right(), Bottom())
	hideydecorations!(ax)
	ax.xlabel = "empirical singular value distributions"
	colsize!(fig.layout, 4, Fixed(1500))

	# Layout
	colgap!(fig.layout, 50)
	colgap!(fig.layout, 3, 100)
	rowgap!(fig.layout, 60)
	resize_to_layout!(fig)

	fig
end

# ╔═╡ 3b1084c0-862d-4b27-89b2-fa8abed05e06
save("figure-04.png", figure_04)

# ╔═╡ Cell order:
# ╟─9b4bb7be-252b-494f-b979-1789941a83bb
# ╠═4acaacbd-196e-428b-b508-0257c2e82bca
# ╠═3f7de967-4f0c-43de-b7d3-b3fa32548079
# ╟─7eb990bc-7c5f-4d1c-8229-ea0189397b64
# ╟─3577f9d8-0d3b-4f92-b3c9-561bd3b45aee
# ╟─9bb27ce0-bf0a-4195-a319-e4e78827b1fd
# ╟─6a73f65c-15dc-4db8-9de2-c69cebe79070
# ╠═4789b052-974d-47a0-bcc8-9d327f2036d3
# ╠═8a8b3358-be9c-4c8a-baf7-6c14c444275a
# ╠═f4af2183-3121-4bf4-8c6b-f6a980136870
# ╟─6f9a6971-825c-48ef-b5bb-16afffdc9ef3
# ╠═55824f36-bde6-4359-b18e-9b23761f35cf
# ╠═3b1084c0-862d-4b27-89b2-fa8abed05e06
